mcnptools
=========

 * srcgen.pl - script to generate srctp files from description
 * mcnpgen.pl - script to generate set of inputs from template
 * mcnprun.pl - script to run a batch of inputs
 * entropy.pl - script to parse entropy from outputs