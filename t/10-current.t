#!/usr/bin/perl

use v5.10;
use warnings;
use English qw(-no_match_vars);

use Test::More tests => 1;
use Test::Script::Run qw(get_perl_cmd);
use IPC::Run;

$, = ' ';

my @cmd = get_perl_cmd('mcnprun.pl', 't/inputs/initial-1.inp');
system @cmd;

ok(1);
