#!/usr/bin/perl

use v5.10;
use warnings;
use English qw(-no_match_vars);

use Test::More tests => 1;
use Test::Script;

use constant SCRIPT => 'blib/script/mcnprun.pl';

script_compiles(SCRIPT, 'MCNPrun compiles');
