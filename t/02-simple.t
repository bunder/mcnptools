#!/usr/bin/perl

use v5.10;
use warnings;
use English qw(-no_match_vars);

use Test::More tests => 2;
use Test::Script::Run;

run_not_ok('mcnprun.pl', undef, 'MCNPrun fails without arguments');
run_ok('mcnprun.pl', ['--help'], 'MCNPrun prints help');
