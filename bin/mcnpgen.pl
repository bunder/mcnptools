#!/usr/bin/perl

# Copyright (c) 2013-2015 by Anton Leontiev <bunder@t-25.ru>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above
#   copyright notice, this list of conditions and the following disclaimer
#   in the documentation and/or other materials provided with the
#   distribution.
# * Neither the name of Anton Leontiev nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use v5.10;
use warnings;
use English qw(-no_match_vars);

use File::Basename;
use Getopt::Std;
use List::Gen qw(range);
use Text::Template;
use JSON qw(to_json);

$OUTPUT_FIELD_SEPARATOR = " ";
$Getopt::Std::STANDARD_HELP_VERSION = 1;
$VERSION = '1.1';

my %args = (
	'i' => "Incremental mode: first file produced from template is complete,\n" .
	       'while next ones are continue',
	'j' => 'Create job file',
	'n:' => 'Specify NPS values.'
);

format option =
@>>>> @<<   ^*
'-' . substr($_, 0, 1), substr($_, 1, 1) eq ':' ? 'arg' : '', $args{$_}
~~          ^*
$args{$_}
.

sub VERSION_MESSAGE {
	say "MCNPgen version $VERSION (c) by Anton Leontiev, 2013-2015";
}

sub HELP_MESSAGE {
	say "\nUsage: mcnpgen.pl [options] [--] [template.tmpl]\n";
	say 'Options:';

	local $FORMAT_NAME = 'option';
	write foreach sort keys %args;

	say qq(\nValues are specified in the format: 'arg[,arg]*', where 'arg');
	say qq(is either a single value or a range in the format: 'start:step:stop');
}

sub create_continue {
	my @allowed = qw(ctme dbcn dd idum fq kcode lost mplot nps prdmp print rdum za zb zc);
	my $re = join '|', @allowed;
	my @out = grep { /^\s*($re)/ } @_;
	unshift @out, 'message: c', '', 'continue';
	return @out;
}

sub parse_range {
	my @t = split ':';
	return $_ if @t == 1;
	die unless @t == 3;
	return @{range $t[0], $t[2], $t[1]};
}

sub nps_suffix {
	return '.' . $_[0] / 1000000 . 'M';
}

getopts(join('', sort keys %args), \%opts);

die "Not enough arguments\n" unless @ARGV;

my @nps = map { parse_range($_) } split ',', $opts{'n'};

while (my $file = shift) {
	unless (-e $file) {
		warn "File '$file' does not exist. Skipping.\n";
		next;
	}

	unless (-r $file) {
		warn "Can not read file '$file'. Skipping.\n";
		next;
	}

	my %job = ('template' => $file);

	$job{'remove'} = [ 'log', 'runtpe' ] if $opts{'i'};

	my ($name, $path, $suffix) = fileparse($file, '.tmpl');
	my $base = $path . $name;

	my $template = Text::Template->new(type => 'file', source => $file, safe => 1) or
		die "Can not create template from file '$file'\n";

	my $first = 1;
	foreach my $nps (@nps) {
		use autodie;

		my $out;
		open my $fh, '>', \$out;
		$template->fill_in(hash => { 'nps' => $nps }, output => $fh);
		close $fh;

		my @out = split /\n/, $out;
		undef $out;

		@out = create_continue(@out) if $opts{'i'} and not $first;

		my $outfilename = $name . nps_suffix($nps) . '.inp';

		local $OUTPUT_FIELD_SEPARATOR = "\n";
		open $fh, '>', $path . $outfilename;
		print $fh @out;
		close $fh;

		push @{$job{'files'}}, $outfilename;
		$first = 0;
	}

	if ($opts{'j'}) {
		use autodie;
		open my $fh, '>', $base . '.mcnpjob';
		print $fh to_json(\%job, { utf8 => 1, pretty => 1 });
		close $fh;
	}
}
