#!/usr/bin/perl

# Copyright 2014, 2015 Anton Leontiev <bunder@t-25.ru>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above
#   copyright notice, this list of conditions and the following disclaimer
#   in the documentation and/or other materials provided with the
#   distribution.
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use v5.10;
use warnings;
use autodie;
use English qw(-no_match_vars);

use Getopt::Std;
use File::Basename qw/basename/;

$VERSION = '1.1';
$OUTPUT_FIELD_SEPARATOR = " ";

$Getopt::Std::STANDARD_HELP_VERSION = 1;

my %args = (
	's' => 'Separate mode: each input file produce its own output file'
);

format option =
@>>>> @<<   ^*
'-' . substr($_, 0, 1), substr($_, 1, 1) eq ':' ? 'arg' : '', $args{$_}
~~          ^*
$args{$_}
.

sub HELP_MESSAGE {
	say "\nUsage: entropy.pl [-s] files\n";

	say 'Options:';
	local $FORMAT_NAME = 'option';
	write foreach sort keys %args;
}

sub VERSION_MESSAGE {
	say 'MCNP entropy parser v' . $VERSION;
	say 'Copyright 2014 Anton Leontiev <bunder@t-25.ru>';
	say 'The BSD license <http://opensource.org/licenses/BSD-3-Clause>';
}

my %opts;
getopts(join('', keys %args), \%opts);

my($cycle, $entropy);

die "No files are specified in command line\n" unless @ARGV;

my $o;

while (my $file = shift) {
	open(my $i, '<:crlf', $file);

	if (not $o or $opts{'s'}) {
		my $outputname = $opts{'s'} ? basename($file, '.txt') . '.entropy.txt' : 'entropy.txt';
		open($o, '>', $outputname);
		say $o '# [Cycle] [Entropy]';
	}

	while (<$i>) {
		chomp;
		if (/estimator +cycle *(\d+|\*+)/) {
			if ($1 =~ /^\*+$/) {
				$cycle++;
			} else {
				warn "Inconsequent cycle number at line $. of $file: $cycle -> $1\n"
					if defined $cycle and $cycle + 1 != $1;
				$cycle = $1;
			}
		}

		if (/source_entropy *([E+.\d]+)$/) {
			$entropy = $1;
			say $o $cycle, $entropy;
		}
	}
}
