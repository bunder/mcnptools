#!/usr/bin/env perl

# Copyright (c) 2013-2015 by Anton Leontiev <bunder@t-25.ru>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above
#   copyright notice, this list of conditions and the following disclaimer
#   in the documentation and/or other materials provided with the
#   distribution.
# * Neither the name of Anton Leontiev nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use v5.10;
use warnings;
no warnings "experimental::smartmatch";
use English qw(-no_match_vars);

use File::Spec::Functions qw(catfile);
use File::Slurp qw(read_file);
use File::Temp qw(tempdir);
use File::Basename;
use File::Which;

use Cwd;
use IO::Tee;
use Getopt::Long;
use Storable;
use JSON qw(from_json);

$OUTPUT_FIELD_SEPARATOR = " ";

$VERSION = '0.3';

sub check_continue {
	use autodie;
	my $file = shift || die;
	open(my $fh, '<', $file);
	my $first = <$fh>;
	close $fh;
	return 1 if $first =~ /^message:\s*cn?/;
	return 0;
}

sub create_continue {
	use autodie;
	my @allowed = qw(ctme dbcn dd idum fq kcode lost mplot nps prdmp print rdum za zb zc);
	my $re = join '|', @allowed;
	open(my $in, '<', shift || die);
	open(my $out, '>', shift || die);
	say $out "message: c\n";
	say $out 'continue';
	local $OUTPUT_FIELD_SEPARATOR = undef;
	print $out grep { /^\s*$re/ } <$in>;
}

sub info {
	state $tee = IO::Tee->new(\*STDOUT, '>> mcnprun.log');
	say $tee '[' . (scalar localtime) . ']', @_;
}

# Return: 1 if MCNP was interrupted by user, 0 otherwise
sub runmcnp {
	use IPC::Run qw(start finish);
	use autodie;

	my $mcnp = shift or die;
	my $rc = 0;

	open my $lf, '>', 'log';

	# MCNP process INT signal itself; we don't want to interfere
	$SIG{'INT'} = 'IGNORE';

	pipe READER, WRITER;
	my $h = start [ $mcnp ], \*STDIN, \*WRITER;
	close WRITER;

	binmode READER, ':crlf';
	while (<READER>) {
		print;
		print $lf $_;
		$rc = 1 if /run terminated by tty interrupt/;
	}

	close READER;
	finish $h;

	# Revert to default behaviour
	$SIG{'INT'} = 'DEFAULT';

	close $lf;
	return $rc;
}

my $mcnp = which('mcnp') or die "Can not find MCNP executable. Stopping.\n";

die "No files specified in command line. Use '" . basename($0) . " -h' for help.\n" unless @ARGV;

my $dir = getcwd;

# First part: parse options and check files

my $job = shift;
die if not defined $job;
$job = from_json(read_file($job));

my $previous;
foreach my $file (@{$job->{'files'}}) {
	my $info = $job->{$file};
	die "File '$file' does not exist.\n" unless -e $file;
	die "Can not read file '$file'.\n" unless -r $file;

	# $opts{continue} indicates if user wants to convert initial input to continue input
	# $opts{iscontinue} indicates if input is already continue

	$info->{iscontinue} = check_continue($file);

	my ($name, $path, $suffix) = fileparse($file, '.inp');

	if ($info->{runtpe} and not $info->{iscontinue} and not $info->{continue}) {
		die "You can not specify runtpe file for complete input file '$file'.\n";
	}

	die "Can not find runtpe file '$opts{runtpe}' for input '$file'.\n"
		if $info->{runtpe} and not -r $info->{runtpe};

	if (not $info->{runtpe} and $info->{iscontinue}) {
		die "You can not specify continue input first: '$file'\n" unless $previous;
		my ($name, $path, $suffix) = fileparse($previous, '.inp');
		# You do not need catfile here. See fileparse description.
		$info->{runtpe} = $path . $name . '.run';
	}

	die "Can not find srctp file '$opts{srctp}' for input '$file'.\n"
		if $info->{srctp} and not -r $info->{srctp};

	$info->{name} = $name;

	# Push collected info back into $job hash
	$job->{$file} = $info;
	$previous = $file;
}

sub copy {
	require File::Copy;

	my ($from, $to) = @_;

	info 'Copying', $from, 'to', $to;
	File::Copy::copy($from, $to) or
		die "Can not copy file '$from' to '$to': $!\n";
}

sub move {
	require File::Copy;

	my ($from, $to) = @_;

	info 'Moving', $from, 'to', $to;
	File::Copy::move($from, $to) or
		die "Can not copy file '$from' to '$to': $!\n";
}


# First part: process jobs

$ENV{'DATAPATH'} = $job->{'datapath'} if defined $job->{'datapath'};

foreach my $file (@{$job->{files}}) {
	my $info = $job->{$file};
	info "Processing '" . $file . "'...";

	my $tempdir = File::Temp->newdir('mcnprun-XXXXXX');

	info "Staring calculation in '$tempdir'";

	if ($info->{runtpe}) {
		my $runtpe = catfile($tempdir, 'runtpe');

		if ('runtpe' ~~ $job->{remove}) {
			move $info->{runtpe}, $runtpe;
		} else {
			copy $info->{runtpe}, $runtpe;
		}
	}

	if ($info->{srctp}) {
		my $srctp = catfile($tempdir, 'srctp');

		if ('srctp' ~~ $job->{remove}) {
			move $info->{srctp}, $srctp;
		} else {
			copy $info->{srctp}, $srctp;
		}
	}

	my $inp = catfile($tempdir, 'inp');

	if ($info->{iscontinue} and not $info->{continue}) {
		create_continue($file, $inp);
	} else {
		copy($file, $inp) or
			die "Can not copy input file '" . $file . "' to '$inp': $!\n";
	}

	store [ $file, $job ], '.current' or
		warn "Can not store process information in '.current': $!\n";

	chdir $tempdir;
	my $rc = runmcnp $mcnp;
	chdir $dir;

	move catfile($tempdir, 'log'),    $info->{name} . '.log' unless 'log' ~~ $job->{remove};
	move catfile($tempdir, 'srctp'),  $info->{name} . '.src';
	move catfile($tempdir, 'outp'),   $info->{name} . '.out';
	move catfile($tempdir, 'runtpe'), $info->{name} . '.run';

	info 'Calculation done';
	unlink '.current';
	exit $rc if $rc;
}
